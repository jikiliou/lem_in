/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/28 21:10:59 by jabadie           #+#    #+#             */
/*   Updated: 2015/10/02 02:59:25 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include "../libft/includes/libft.h"
#include <stdio.h>

# define NB_ANT		3

typedef struct s_way	t_way;

typedef struct	s_room
{
	char			*name;
	int				coord[2];
	t_way			*link;
}				t_room;

struct	s_way
{
	t_room			*room;
	struct s_way	*next;
};

typedef struct s_map
{
	t_room	*start;
	t_room	*end;
	t_way	*all_node; // Liste de tous les noeuds (peut-etre inutile en dehors du parsing)
}				t_map;

typedef struct	s_allway
{
	int				rentability;
	int				way_len;
	t_way			*way;
	struct s_allway	*next;
}				t_allway;

typedef struct	s_combi
{
	int				nb_turn;
	int				nb_way;
	t_allway		*allway;
}				t_combi;

typedef struct	s_ant
{
	t_room	*actual_room;
	t_way	*next_room;
}				t_ant;

typedef struct	s_env
{
	int			start;
	int			end;
	t_map		map;
	t_combi		*combi;
	t_allway	*all_way; // Liste
	t_ant		*ants; // Tableau
}				t_env;

void	execution(t_env *env, int nb_ants);

t_combi		*find_the_combination(t_allway *allway, int nb_max);
void		find_every_way(t_env *env);

t_way		*copy_way(t_way *original);
t_way		*create_way(t_room *room);
t_allway	*copy_allway(t_allway *original);
t_allway	*create_allway(t_way *way, int len);

void		pars_connection(t_env *env, char *line);
void		pars_room(t_env *env, char *line);
void		parser(t_env *env);

void	calc_combi_rentability(t_combi *combi);
void	clean_combi(t_combi *combi);
void	calc_combi_turn(t_combi *combi);

void	print_way(t_way *room);
void	print_allway(t_allway *all);
void	print_combi(t_combi *combi);
#endif

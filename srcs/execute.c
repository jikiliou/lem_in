/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <jabadie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/02 02:03:33 by jabadie           #+#    #+#             */
/*   Updated: 2015/10/02 03:00:13 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	create_ants(t_env *env)
{
	int	i;

	if (!(env->ants = ft_memalloc(sizeof(t_ant) * NB_ANT)))
		ft_error("Malloc error in create_ants");
	i = 0;
	while (i < NB_ANT)
		env->ants[i++].actual_room = env->map.start;
	env->start = NB_ANT;
	env->end = 0;
}

void	actualise_ants(t_env *env, int *nb_ants)
{
	int		i;

	i = 0;
	while (i < NB_ANT)
	{
		if (env->ants[i].next_room != NULL)
		{
			env->ants[i].actual_room = env->ants[i].next_room->room;
			printf("L%d-%s ", i + 1, env->ants[i].actual_room->name);
			env->ants[i].next_room = env->ants[i].next_room->next;
			if (env->ants[i].next_room == NULL)
			{
				env->end++;
				(*nb_ants)--;
			}
		}
		i++;
	}
	printf("\n");
}

void	execution(t_env *env, int nb_ants)
{
	t_allway	*tmp;

	printf("Start Execution\n");
	create_ants(env);
	while(nb_ants > 0)
	{
		tmp = env->combi->allway;
		while (tmp != NULL && env->start > 0)
		{
			if (env->start >= tmp->rentability)
			{
				env->ants[NB_ANT - env->start].next_room = tmp->way->next;
				env->start--;
			}
			tmp = tmp->next;
		}
		actualise_ants(env, &nb_ants);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_way_rentability.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/30 02:03:22 by jabadie           #+#    #+#             */
/*   Updated: 2015/10/02 02:00:54 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"


void	calc_combi_rentability(t_combi *combi)
{
	t_allway	*tmp;
	t_allway	*other;
	int			r;

	tmp = combi->allway;
	while (tmp->next != NULL)
	{
		other = tmp->next;
		r = 0;
		while (other != NULL)
		{
			r += tmp->way_len - other->way_len + 1;
			other = other->next;
		}
		tmp->rentability = r;
		tmp = tmp->next;
	}
	tmp->rentability = 1;
}

void	clean_combi(t_combi *combi)
{
	while (combi->allway->rentability > NB_ANT)
	{
		combi->allway = combi->allway->next;
		combi->nb_way--;
	}
}

void	calc_combi_turn(t_combi *combi)
{
	t_allway	*tmp;
	int			nb_ants;
	int			nb_turn;
	int			nb_way;

	nb_ants = NB_ANT;
	nb_way = combi->nb_way;
	nb_turn = 0;
	tmp = combi->allway;
	nb_ants -= tmp->rentability;
	nb_turn += tmp->way_len;
	while (nb_ants > 0)
	{
		while (tmp->rentability <= nb_ants)
		{
			nb_ants -= nb_way;
			nb_turn++;
		}
		while (combi->allway->rentability > nb_ants && nb_ants > 0)
		{
			tmp = tmp->next;
			nb_way--;
		}
	}
	combi->nb_turn = nb_turn;
}

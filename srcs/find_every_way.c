/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_every_possible_way.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <jabadie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 20:04:21 by jabadie           #+#    #+#             */
/*   Updated: 2015/10/02 03:05:19 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		room_already_tested(t_way *lst, char *name)
{
	while (lst != NULL)
	{
		if (ft_strcmp(lst->room->name, name) == 0)
			return (1);
		lst = lst->next;
	}
	return (0);
}

void	add_this_way(t_env *env, t_way *first, int len)
{
	t_allway	*before;
	t_allway	*allway;

	if (env->all_way == NULL)
		env->all_way = create_allway(first, len);
	else
	{
		before = NULL;
		allway = env->all_way;
		while (allway->next != NULL)
		{
			if (len < allway->way_len)
			{
				if (before == NULL)
				{
					env->all_way = create_allway(first, len);
					env->all_way->next = allway;
				}
				else
				{
					before->next = create_allway(first, len);
					before->next->next = allway;
				}
				return ;
			}
			before = allway;
			allway = allway->next;
		}
		allway->next = create_allway(first, len);
	}
}

void	find_them(t_env *env, t_way *first, t_way *actual, int len)
{
	t_way	*tmp;

	if (actual->room == env->map.end)
		add_this_way(env, first, len);
	else
	{
		tmp = actual->room->link;
		while (tmp != NULL)
		{
			if (room_already_tested(first, tmp->room->name) == 0)
			{
				actual->next = create_way(tmp->room);
				find_them(env, first, actual->next, len + 1);
				free(actual->next);
				actual->next = NULL;
			}
			tmp = tmp->next;
		}
	}
}

void	find_every_way(t_env *env)
{
	t_way	*first;

	first = create_way(env->map.start);
	find_them(env, first, first, 0);
	if (env->all_way == NULL)
		ft_error("No way from the start to the end.");
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/28 21:29:25 by jabadie           #+#    #+#             */
/*   Updated: 2015/09/29 20:11:45 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	pars_command(t_env *env, char *line)
{
	if (ft_strstr("##start", line) != NULL)
		env->start = 1;
	else if (ft_strstr("##end", line) != NULL)
		env->end = 1;
}

void	parser(t_env *env)
{
	char	*line;
	int		nb_words;

	while (get_next_line(0, &line) > 0)
	{
		if (line[0] == '#')
			pars_command(env, line);
		else if ((nb_words = find_nb_words(line, ' ')) == 3)
			pars_room(env, line);
		else if (nb_words == 1)
			pars_connection(env, line);
		else
			ft_complex_error("Wrong Argument in the at this line: ", line);
		free(line);
	}
}

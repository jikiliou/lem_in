/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_connection.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <jabadie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 19:00:56 by jabadie           #+#    #+#             */
/*   Updated: 2015/09/29 20:11:34 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_room	*find_room(t_way *all_node, char *name)
{
	while(all_node != NULL)
	{
		if (ft_strcmp(all_node->room->name, name) == 0)
			return (all_node->room);
		all_node = all_node->next;
	}
	return (NULL);
}

void	add_link(t_room *room1, t_room *room2)
{
	t_way	*tmp;
	t_way	*link_room2;

	link_room2 = create_way(room2);
	if (room1->link == NULL)
		room1->link = link_room2;
	else
	{
		tmp = room1->link;
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = link_room2;
	}
}

void	pars_connection(t_env *env, char *line)
{
	char	**res;
	t_room	*room1;
	t_room	*room2;

	res = ft_strsplit(line, '-');
	room1 = find_room(env->map.all_node, res[0]);
	room2 = find_room(env->map.all_node, res[1]);
	free_chch(res);
	if (room1 == NULL || room2 == NULL)
		ft_complex_error("Problem in Connection at this line : ", line);
	add_link(room1, room2);
	add_link(room2, room1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   way_utilities.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <jabadie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/02 01:54:34 by jabadie           #+#    #+#             */
/*   Updated: 2015/10/02 01:58:32 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_way	*create_way(t_room *room)
{
	t_way	*new;

	if (!(new = ft_memalloc(sizeof(t_way))))
		ft_error("Malloc error in create_way");
	new->room = room;
	return (new);
}

void		free_way(t_way *way)
{
	t_way *after;

	while (way != NULL)
	{
		after = way->next;
		free(way);
		way = after;
	}
}

t_way	*copy_way(t_way *original)
{
	t_way	*new;
	t_way	*first;

	new = create_way(original->room);
	first = new;
	original = original->next;
	while (original != NULL)
	{
		new->next = create_way(original->room);
		new = new->next;
		original = original->next;
	}
	return (first);
}

t_allway	*create_allway(t_way *way, int len)
{
	t_allway	*new;

	if (!(new = ft_memalloc(sizeof(t_allway))))
		ft_error("Malloc error in create_allway");
	new->way = copy_way(way);
	new->way_len = len;
	return (new);
}

t_allway	*copy_allway(t_allway *original)
{
	t_allway	*copy;

	if (!(copy = malloc(sizeof(t_allway))))
		ft_error("Malloc error in copy_allway");
	copy->way = original->way;
	copy->way_len = original->way_len;
	copy->rentability = original->rentability;
	copy->next = NULL;
	return (copy);
}

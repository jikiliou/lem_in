/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/28 21:26:13 by jabadie           #+#    #+#             */
/*   Updated: 2015/10/02 03:06:50 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"
#include <stdio.h>

void	print_way(t_way *room)
{
	t_way *tmp;

	tmp = room;
	while (tmp != NULL)
	{
		printf("%s ", tmp->room->name);
		tmp = tmp->next;
	}
	printf("\n");
}

void	print_map(t_map *map)
{
	t_way	*tmp;

	tmp = map->all_node;
	while (tmp != NULL)
	{
		printf("Room : %s\n", tmp->room->name);
		printf("This room have links with : ");
		print_way(tmp->room->link);
		tmp = tmp->next;
	}
	printf("Start Room : %s\n", map->start->name);
	printf("End Room : %s\n", map->end->name);
}

void	print_allway(t_allway *all)
{
	while (all != NULL)
	{
		printf("Way with len : %d ; rentability : %d\n", all->way_len, all->rentability);
		print_way(all->way);
		all = all->next;
	}
}

void	print_combi(t_combi *combi)
{
	printf("Combi : NB_turn : %d, Nb_way : %d\n", combi->nb_turn, combi->nb_way);
	print_allway(combi->allway);
}

int		nb_link(t_way *link)
{
	int	nb;

	nb = 0;
	while (link != NULL)
	{
		nb++;
		link = link->next;
	}
	return (nb);
}

int		calc_max_way(t_env *env)
{
	int	nb_start;
	int	nb_end;

	nb_start = nb_link(env->map.start->link);
	nb_end = nb_link(env->map.end->link);
	return (nb_start > nb_end ? nb_end : nb_start);
}

int		main(void)
{
	t_env	env;

	parser(&env);
	print_map(&env.map);
	find_every_way(&env);
	print_allway(env.all_way);

	printf("\n\n--------------------\n\n");
	printf("MAx Way : %d\n", calc_max_way(&env));

	env.combi = find_the_combination(env.all_way, calc_max_way(&env));
	print_combi(env.combi);
	execution(&env, NB_ANT);
	return (0);
}

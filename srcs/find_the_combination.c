/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   epure_some_way.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 23:46:07 by jabadie           #+#    #+#             */
/*   Updated: 2015/10/02 03:10:15 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			cmp_way(t_way *way1, t_way *way2)
{
	t_way	*save;

/*	printf("way1 : ");
	print_way(way1);
	printf("way2 : ");
	print_way(way2);
*/	way1 = way1->next;
	way2 = way2->next;
	save = way2;
	while (way1->next != NULL) // Le ->next est la pour eviter de tester la salle END
	{
		way2 = save;
		while (way2->next != NULL) // Pareil ici
		{
			if (way1->room == way2->room)
				return (1);
			way2 = way2->next;
		}
		way1 = way1->next;
	}
	return (0);
}

int			cmp_combi(t_combi *combi, t_way *way)
{
	t_allway	*tmp;

	tmp = combi->allway;
	while (tmp != NULL)
	{
		if (cmp_way(tmp->way, way) == 1)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

t_combi		*create_comb(void)
{
	t_combi	*new;

	if (!(new = ft_memalloc(sizeof(t_combi))))
		ft_error("Malloc error in create_comb");
	return (new);
}

void		add_way_to_comb(t_combi *combi, t_allway *allway)
{
	t_allway	*tmp;

	combi->nb_way++;
	if (combi->allway == NULL)
		combi->allway = copy_allway(allway);
	else
	{
		tmp = combi->allway;
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = copy_allway(allway);
	}
}

void	reverse_order(t_combi *combi)
{
	t_allway	*tmp;
	t_allway	*before;
	t_allway	*after;

	before = NULL;
	tmp = combi->allway;
	while (tmp != NULL)
	{
		after = tmp->next;
		tmp->next = before;
		before = tmp;
		tmp = after;
	}
	combi->allway = before;
}

int			new_combi_best(t_combi *best, t_combi *actual)
{
	reverse_order(actual);
	calc_combi_rentability(actual);
	clean_combi(actual);
	calc_combi_turn(actual);
	if (best == NULL || best->nb_turn > actual->nb_turn)
	{
		printf("COMBINATION VALIDATION\n");
		return (1);;
	}
	printf("FAILED COMBINATION\n");
	return (0);
}

t_combi		*find_the_combination(t_allway *allway, int nb_max)
{
	t_allway	*to_compare;
	t_combi		*best_comb;
	t_combi		*actual_comb;

	best_comb = NULL;
	actual_comb = create_comb();
	if (allway->next == NULL || nb_max == 1)
	{
		actual_comb->allway = copy_allway(allway);
		return (actual_comb);
	}
	while (allway->next != NULL)
	{
		//print_allway(first);
		add_way_to_comb(actual_comb, allway);
		to_compare = allway->next;
		while (to_compare != NULL)
		{
			if (cmp_combi(actual_comb, to_compare->way) == 0) // A Changer Cmp_combi
			{
				add_way_to_comb(actual_comb, to_compare);
				if (actual_comb->nb_way == nb_max)
					break ;
			}
			to_compare = to_compare->next;
		}
		if (new_combi_best(best_comb, actual_comb) == 1)
			best_comb = actual_comb;
		else
			free(actual_comb);
		actual_comb = create_comb();
		allway = allway->next;
	}
	free(actual_comb);
	return (best_comb);
}

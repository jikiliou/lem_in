/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_room.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jabadie <jabadie@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/29 18:48:35 by jabadie           #+#    #+#             */
/*   Updated: 2015/10/02 01:54:51 by jabadie          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_room	*create_room(char *line)
{
	t_room	*new;
	char	**res;

	if (!(new = ft_memalloc(sizeof(t_room))))
		ft_error("Malloc error in create_room");
	res = ft_strsplit(line, ' ');
	new->name = ft_strdup(res[0]);
	new->coord[0] = ft_atoi(res[1]);
	new->coord[1] = ft_atoi(res[2]);
	free_chch(res);
	return (new);
}

void	pars_room(t_env *env, char *line)
{
	static t_way	*last_room_added;
	t_way			*room;

	room = create_way(create_room(line));
	if (env->start == 1)
		env->map.start = room->room;
	else if (env->end == 1)
		env->map.end = room->room;
	if (env->map.all_node == NULL)
	{
		env->map.all_node = room;
		last_room_added = env->map.all_node;
	}
	else
	{
		last_room_added->next = room;
		last_room_added = last_room_added->next;
	}
	env->start = 0;
	env->end = 0;
}
